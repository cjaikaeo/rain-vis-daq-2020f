Rain API Server with Visualization Examples
===========================================

An example project for RESTful API development using OpenAPI and Python.  It contains a few examples for data visualization on a web browser.
